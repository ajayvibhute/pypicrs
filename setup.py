#!/usr/bin/env python
from setuptools import setup, Extension




setup(
    author="Ajay Vibhute",
    author_email="ajayvibhute@gmail.com",
    name="picrs",
    version="0.0.1",
    url="add url",
    description="Semi-automated tool to help classify extended Radio sources",
    packages=[],
    setup_requires=["bdsf","astropy","reproject","mpl_interactions"],
    install_requires=["bdsf","astropy","reproject","mpl_interactions"],
    python_requires=">=3.6"
)

"""
setuptools.setup(
    name="mypackage",
    version="0.0.1",
    author="Example Author",
    author_email="author@example.com",
    description="A small example package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    project_urls={
        "Bug Tracker": "https://github.com/pypa/sampleproject/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
)
"""
