"""" 
    picrs_util.py: The module contains all utility routines required to process the data
"""

import requests
import numpy as np
from astropy.table import Table
from astropy.io import fits
import time
from io import StringIO
from astropy.wcs import WCS
from reproject import reproject_interp

def tranformFirstImage(filename,hdu,overwrite=False):
    """
        transformFirstImage: The dimensions (number of pixels) of first image and ps1 image are not same.
        This function reprojects the first image as per ps1 image dimensions and update the file

        filename: Name of the input file which needs to be reprojected
        hdu: fits hdu for reprojecting the data
        overwrite: Update the input file, if set to false, creates a new file with <input_file_name>_updated.fits

        returns name of the reprojected file
    """
    fh=fits.open(filename)

    fh[0].header.remove("CTYPE3");
    fh[0].header.remove("CTYPE4");
    fh[0].header.remove("CRVAL3");
    fh[0].header.remove("CRVAL4");
    fh[0].header.remove("CDELT3");
    fh[0].header.remove("CDELT4");
    fh[0].header.remove("CRPIX4");
    fh[0].header.remove("CRPIX3");
    fh[0].header.remove("CROTA3");
    fh[0].header.remove("CROTA4");
    array, footprint = reproject_interp(fh, hdu.header,order='nearest-neighbor')
    fh=fits.open(filename)    
    ph=fits.PrimaryHDU(data=array,header=fh[0].header)
    phlist=fits.HDUList([ph])
    
    if overwrite:
        outfile=filename
    else:
        outfile=filename.replace(".fits","")+"_updated.fits"

        phlist.writeto(outfile,overwrite=True)

    return outfile
        



def getWCS(filename,naxis=2):
    """
        The function returns WCS coordinate system
        filename: The name of the fits file
        naxis: Number of axis to be used for WCS, default is 2

        returns an object of the wcs 
    """
    hdu=fits.open(filename)
    return WCS(hdu[0].header,naxis=naxis)
    """
    w=Wcsprm()
    w.crpix=[hdu[0].header["CRPIX1"],hdu[0].header["CRPIX2"]]
    w.crval=[hdu[0].header["CRVAL1"],hdu[0].header["CRVAL2"]]
    w.cdelt=[hdu[0].header["CDELT1"],hdu[0].header["CDELT2"]]
    return w
   """ 

def getImage(filename):
   hdu=fits.open(filename)
   img=hdu[0].data
   return img,hdu[0]

def getFirstImageCutout(tra,tdec,tsize=30):
    
    """
    Downloads the radio image cutout from FIRST survey in FITS format
    tra, tdec = list of positions in degrees
    size = image size in arcmin

    Returns path of the downloaded fits file
    """

    firsturl="https://third.ucllnl.org/cgi-bin/firstcutout?RA="+str(tra)+"%20"+str(tdec)+"&ImageType='FitsFile'&ImageSize="+str(tsize)+"Equinox=J2000"
    r=requests.get(firsturl)
    outfilename=""
    if(r.status_code==200):
        outfilename="first_"+str(tra)+"_"+str(tdec)+".fits"
        open(outfilename,"wb").write(r.content)
    else:
        outfilename=None

    return outfilename


def HMS2deg(ra='', dec=''):
  RA, DEC, rs, ds = '', '', 1, 1
  if dec:
    D, M, S = [float(i) for i in dec.split()]
    if str(D)[0] == '-':
      ds, D = -1, abs(D)
    deg = D + (M/60) + (S/3600)
    DEC = '{0}'.format(deg*ds)
  
  if ra:
    H, M, S = [float(i) for i in ra.split()]
    if str(H)[0] == '-':
      rs, H = -1, abs(H)
    deg = (H*15) + (M/4) + (S/240)
    RA = '{0}'.format(deg*rs)
  
  if ra and dec:
    return (RA, DEC)
  else:
    return RA or DEC


#coode credit: PS1 Image cutout service, https://outerspace.stsci.edu/display/PANSTARRS/PS1+Image+Cutout+Service 
def getimages(tra, tdec, size=240, filters="grizy", format="fits", imagetypes="stack"):
     
    """Query ps1filenames.py service for multiple positions to get a list of images
    This adds a url column to the table to retrieve the cutout.
     
    tra, tdec = list of positions in degrees
    size = image size in pixels (0.25 arcsec/pixel)
    filters = string with filters to include
    format = data format (options are "fits", "jpg", or "png")
    imagetypes = list of any of the acceptable image types.  Default is stack;
        other common choices include warp (single-epoch images), stack.wt (weight image),
        stack.mask, stack.exp (exposure time), stack.num (number of exposures),
        warp.wt, and warp.mask.  This parameter can be a list of strings or a
        comma-separated string.
 
    Returns an astropy table with the results
    """
     
    ps1filename = "https://ps1images.stsci.edu/cgi-bin/ps1filenames.py"
    fitscut = "https://ps1images.stsci.edu/cgi-bin/fitscut.cgi"
    if format not in ("jpg","png","fits"):
        raise ValueError("format must be one of jpg, png, fits")
    # if imagetypes is a list, convert to a comma-separated string
    if not isinstance(imagetypes,str):
        imagetypes = ",".join(imagetypes)
    # put the positions in an in-memory file object
    cbuf = StringIO()
    cbuf.write('\n'.join(["{} {}".format(ra, dec) for (ra, dec) in zip(tra,tdec)]))
    cbuf.seek(0)
    # use requests.post to pass in positions as a file
    r = requests.post(ps1filename, data=dict(filters=filters, type=imagetypes),
        files=dict(file=cbuf))
    r.raise_for_status()
    tab = Table.read(r.text, format="ascii")
 
    urlbase = "{}?size={}&format={}".format(fitscut,size,format)
    tab["url"] = ["{}&ra={}&dec={}&red={}".format(urlbase,ra,dec,filename)
            for (filename,ra,dec) in zip(tab["filename"],tab["ra"],tab["dec"])]
    return tab

def getPS1ImageCutout(tra,tdec,tsize=450,filter="z"):
    """
    Downloads the optical image cutout from PS1 Image Cutout Service in FITS format
    tra, tdec = list of positions in degrees
    size = image size in arcmin, default is 450
    filters = string with filters to include, default is 'z'
    Returns path of the downloaded fits file
    """

    outfilename=None

    table=getimages(tra,tdec,filters="z",size=tsize)
    #add condition to check if we get multiple images, 
    #need to check how to process that condition
    for row in table:
        ra = row['ra']
        dec = row['dec']
        #projcell = row['projcell']
        #subcell = row['subcell']
        #filter = row['filter']
        url = row["url"]
        r = requests.get(url)
        outfilename="ps1_"+str(ra)+"_"+str(dec)+".fits"
        open(outfilename,"wb").write(r.content)

    return outfilename

