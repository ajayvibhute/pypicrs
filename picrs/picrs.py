import matplotlib.pyplot as plt
import numpy as np
import matplotlib.cm as cm
import bdsf
from matplotlib.patches import Ellipse
from matplotlib import collections
from . import picrs_utils as pu
from mpl_interactions import ioff, panhandler, zoom_factory
from astropy.wcs.utils import pixel_to_skycoord
from astropy.stats import sigma_clipped_stats

def printme():
    print("Welcoome")
class PICRS:

    def __init__(self,tra,tdec,size=1):
        self.tra=tra
        self.tdec=tdec
        self.size=size
        self.img=None
        self.ps1_img_data=None
        self.wcs=None
        self.source_length=0

    def processImage(self):
        """
        Function takes position (ra,dec) in degree and downloads a radio image cutout from 
        the FIRST survey and optical image from the PS1 Image cutout service. Then the module
        processes the radio image using PyBDSF and generates a plot. The optical image is 
        plotted after initial cleaning.
        tra, tdec = list of positions in degrees
    
        Generates the optical and radio image plots

        """
        size=self.size #arcmin
        #firstimage="first_00 12 09.85_09 02 49.3.fits"
        #ps1image="ps1_3.04104166667_9.04702777778.fits"

        tra=self.tra
        tdec=self.tdec
        tra_deg,tdec_deg=pu.HMS2deg(tra,tdec) 
        tra_deg=[np.float64(tra_deg)]
        tdec_deg=[np.float64(tdec_deg)]
        ps1image=pu.getPS1ImageCutout(tra_deg,tdec_deg,np.int32(size*60.0/0.25))
        if(ps1image==None):
            print("Unable to download image cutout from the PS1 Image cutout service")
    
        self.ps1_img_data,hdu=pu.getImage(ps1image)


        firstimage=pu.getFirstImageCutout(tra,tdec,size)
        if(firstimage==None):
            print("Unable to download image cutout from the FIRST survey")

        first_tranformed=pu.tranformFirstImage(firstimage,hdu)
        print("Transformed File:",first_tranformed)
        self.img=bdsf.process_image(first_tranformed)


        self.wcs=[]
        self.wcs.append(pu.getWCS(firstimage))
        self.wcs.append(pu.getWCS(ps1image))
    
    #plotImage(img,ps1_img_data,self.wcs)
    
#def plotimage(img,ps1_img_data,wcs):
    def plot(self):
        #write logic to enforce sequence
        """
            Plots the processed image and allows to compute the distance between sources
        """
        img=self.img
        ps1_img_data=self.ps1_img_data
        wcs=self.wcs

        island_offsets_x = []
        island_offsets_y = []
        markers = []
        colours = ['m', 'b', 'c', 'g', 'y', 'k'] # reserve red ('r') for wavelets 
        styles = ['-', '-.', '--']
        border_color = []
        #ax = plt.gca()
        #fig,ax=plt.subplots(1, 2,projection=wcs)
        with ioff:
            ax1 = plt.subplot(121,projection=wcs[0])
            ax2 = plt.subplot(122,sharex=ax1,sharey=ax1,projection=wcs[0])
            #ax1=plt.subplot(121,projection=wcs[0])
            #ax2=plt.subplot(122,projection=wcs[1])
            for iisl, isl in enumerate(img.islands):
                xb, yb = isl.border
                if hasattr(isl, '_pi'):
                    for c in range(len(xb)):
                        border_color.append('r')
                else:
                    for c in range(len(xb)):
                        border_color.append('#afeeee')
            island_offsets_x += xb.tolist()
            island_offsets_y += yb.tolist()
            marker = ax1.text(np.max(xb)+2, np.max(yb), str(isl.island_id),color='#afeeee', clip_on=True)
            marker.set_visible(not marker.get_visible())
            markers.append(marker)
            # draw the gaussians with one colour per source or island
            if hasattr(img, 'nsrc'):
                nsrc = len(isl.sources)
                for isrc in range(nsrc):
                    col = colours[int(isrc % 6)]
                    style = styles[int(isrc/6 % 3)]
                    src = isl.sources[isrc]
                    for g in src.gaussians:
                        if hasattr(g, 'valid'):
                            valid = g.valid
                        else:
                            valid = True
                        if g.jlevel == 0 and valid and g.gaus_num >= 0:
                            gidx = g.gaus_num
                            num=np.minimum(g.size_pix[0],g.size_pix[1])
                            denom=np.maximum(g.size_pix[0],g.size_pix[1])
                            e = Ellipse(xy=g.centre_pix, width=g.size_pix[0],height=g.size_pix[1], angle=g.size_pix[2]+90.0)
                            ax1.add_artist(e)
                            e.set_picker(3)
                            e.set_clip_box(ax1.bbox)
                            e.set_facecolor(col)
                            e.set_alpha(0.5)
                            e.gaus_id = gidx
                            e.src_id = src.source_id
                            e.jlevel = g.jlevel
                            e.isl_id = g.island_id
                            e.tflux = g.total_flux
                            e.pflux = g.peak_flux
                            e.centre_sky = g.centre_sky
        if len(img.islands) > 0:
               island_offsets = list(zip(np.array(island_offsets_x), np.array(island_offsets_y)))
               isl_borders = collections.AsteriskPolygonCollection(4, offsets=island_offsets, color=border_color,transOffset=ax1.transData, sizes=(10.0,))
               isl_borders1 = collections.AsteriskPolygonCollection(4, offsets=island_offsets, color=border_color,transOffset=ax2.transData, sizes=(10.0,))
               ax1.add_collection(isl_borders)
               ax2.add_collection(isl_borders1)

        im_mean = img.clipped_mean
        im_rms = img.clipped_rms
        gray_palette = cm.gray
        gray_palette.set_bad('k')

        if img.resid_gaus_arr is None:
            low = 1.1*abs(img.min_value)
        else:
            low = np.max([1.1*abs(img.min_value),1.1*abs(np.nanmin(img.resid_gaus_arr))])
        min_est = im_mean - im_rms*5.0 + low
        vmin_est = im_mean - im_rms*5.0 + low

        if vmin_est <= 0.0:
            vmin = np.log10(low)
        else:
            vmin = np.log10(vmin_est)
        vmax = np.log10(im_mean + im_rms*30.0 + low)

        #im=img.resid_gaus_arr
        im=np.log10(img.ch0_arr + low)
        origin = 'lower'


        ax1.set_xlabel("RA")
        ax1.set_ylabel("DEC")

        ax2.set_xlabel("RA")
        ax2.set_ylabel("DEC")

        ax1.imshow(np.transpose(im), origin=origin, interpolation='nearest', cmap=gray_palette)#,vmin=vmin, vmax=vmax)
        mean,median,sigma=sigma_clipped_stats(ps1_img_data)
        vmin=mean-3*sigma
        vmax=mean+3*sigma
        ax2.imshow(ps1_img_data, origin=origin, cmap=gray_palette,vmin=vmin, vmax=vmax)


        disconnect_zoom = zoom_factory(ax1)
        disconnect_zoom = zoom_factory(ax2)


        #display(ax1.figure.canvas)
        #display(ax2.figure.canvas)

        pan_handler = panhandler(ax1.figure)


        #display(ax1.figure)
        #display(ax2.figure)
    
        pts = []
        while len(pts) < 3:
            print("In looop")
            pts = np.asarray(plt.ginput(n=3))
            if len(pts)==3:
                skypts=pixel_to_skycoord(pts[:,0],pts[:,1],wcs[0])
                ax1.plot([skypts.ra[0].deg,skypts.dec[0].deg],[skypts.ra[1].deg,skypts.dec[1].deg])
                fig = ax1.get_figure() 
                fig.canvas.draw()

        self.source_length=skypts[0].separation(skypts[1])+skypts[1].separation(skypts[2])
        print("Length:",self.source_length.arcmin)
        plt.show()
    
    #defining

""" 
if __name__ == "__main__":
    processImage("00 12 09.85","09 02 49.3")

    """
