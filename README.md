# PyPICRS

Python tool to find extended radio sources

## Installation
git clone https://gitlab.com/ajayvibhute/pypicrs.git

cd pypicrs

python setup.py install

## Usage
1. Import the package

from picrs import picrs

2. Initilise the environment by giving target source coordinate

pp=picrs.PICRS("00 12 09.85","09 02 49.3")

Above command takes "size" as an optional argument. By default the size is 1 arcmin, you can change it using

pp=picrs.PICRS("00 12 09.85","09 02 49.3",size=5)
 
3. Process image 
 pp.processImage()

4. Plot image

pp.plot()




## Authors and acknowledgment
This packages acknowledges use of the PyBDSF, First and PS1 image cutout services

Ajay Vibhute, Pratik Dabhade
Shishir Sankhyan, Tejas Kale

## License
For open source projects, say how it is licensed.

## Project status
Initial version of the package
